const { knex } = require('../../knex');

class authenModel {
  onSaveAuthen({ dplusId, deviceId, accessToken }) {
    return knex.raw(`EXEC DDMS_Authen_UpsertLogin ?, ?, ?`, [
      dplusId,
      deviceId,
      accessToken,
    ]);
  }

  onCheckAccessToken({ accessToken }) {
    return knex.raw(`EXEC DDMS_Authen_CheckAccessToken ?`, [accessToken]);
  }

  onLogout({ accessToken }) {
    return knex.raw(`EXEC DDMS_Authen_Logout ?`, [accessToken]);
  }
}

module.exports = new authenModel();
