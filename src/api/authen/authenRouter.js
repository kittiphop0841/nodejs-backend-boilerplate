const authenRouter = require('express')['Router']();
const { validateSchema } = require('../../middleware/validate-schema.js');
const authenController = require('./authenController.js');
const { schemaLoginGoogle, schemaLogin } = require('./authenSchema.js');

authenRouter.post(
  '/login-google',
  validateSchema([schemaLoginGoogle]),
  authenController.onLoginGoogle,
);

authenRouter.post(
  '/login',
  validateSchema([schemaLogin]),
  authenController.onLogin,
);

authenRouter.post('/logout', authenController.onLogout);

authenRouter.get('/profile', authenController.onGetProfileCurrent);

authenRouter.post('/generate', authenController.onGenerateAuth);

module.exports = authenRouter;
