const { readFileSync } = require('fs');
const jsonwebtoken = require('jsonwebtoken');
const path = require('path');

exports.generateAccessToken = (objToken) => {
  const pathTokenKey = path.join(
    __dirname + '../../../utils/key/sign/privateKey.pem',
  );
  const privateKey = readFileSync(pathTokenKey);
  return jsonwebtoken.sign(objToken, privateKey, {
    algorithm: 'RS256',
    expiresIn: '150d',
  });
};
