const Joi = require('joi');

exports.schemaLoginGoogle = Joi.object().keys({
  auth: Joi.string().required(),
});

exports.schemaLogin = Joi.object().keys({
  username: Joi.string().required(),
  password: Joi.string().required(),
});
