const { readFileSync } = require('fs');
const path = require('path');
const { v1: uuidv4 } = require('uuid');
const { failed, success } = require('../../config/response');
const { decryptedData, encryptedData } = require('../../functions/encrypt');
const moment = require('moment');
const { generateAccessToken } = require('./authenFn');
const authenModel = require('./authenModel');

class authenController {
  async onLoginGoogle(req, res) {
    try {
      const { auth, deviceId } = req.body;
      const pathFileKey = path.join(
        __dirname + '../../../utils/key/auth/privateKey.pem',
      );
      const privKey = readFileSync(pathFileKey, 'utf-8');
      const decrypt = decryptedData(privKey, auth);
      if (!decrypt || !decrypt.success) {
        return failed(req, res, 'เข้าสู่ระบบไม่สำเร็จ (90090)');
      }
      delete decrypt.success;
      const { dplus_id: dplusId, expired_date } = decrypt;
      const currentDate = moment().format('YYYY-MM-DD HH:mm:ss');
      const expireDate = moment(expired_date).format('YYYY-MM-DD HH:mm:ss');
      if (currentDate > expireDate) {
        return failed(req, res, 'เข้าสู่ระบบไม่สำเร็จ ไม่สามารถใช้ได้ (90091)');
      }
      const objToken = { dplusId, loginDate: moment().format() };
      const accessToken = generateAccessToken(objToken);
      await authenModel.onSaveAuthen({
        dplusId,
        deviceId: deviceId || uuidv4(),
        accessToken,
      });
      success(res, 'เข้าสู่ระบบสำเร็จ', {
        accessToken,
      });
    } catch (error) {
      failed(req, res, 'เข้าสู่ระบบไม่สำเร็จ', error);
    }
  }

  async onLogin(req, res) {
    try {
      const { username, password, deviceId } = req.body;
      if (username !== 'demo@dplusonline.com' && password !== 'demo1234!') {
        return failed(req, res, 'เข้าสู่ระบบไม่สำเร็จ', {});
      }
      const dplusId = 'ZT00000';
      const objToken = { dplusId, loginDate: moment().format() };
      const accessToken = generateAccessToken(objToken);
      await authenModel.onSaveAuthen({
        dplusId,
        deviceId: deviceId || uuidv4(),
        accessToken,
      });
      success(res, 'เข้าสู่ระบบสำเร็จ', {
        accessToken,
      });
    } catch (error) {
      failed(req, res, 'เข้าสู่ระบบไม่สำเร็จ', error);
    }
  }

  async onGetProfileCurrent(req, res) {
    try {
      success(res, 'ดึงข้อมูลโปรไฟล์สำเร็จ');
    } catch (error) {
      failed(req, res, 'ดึงข้อมูลโปรไฟล์ไม่สำเร็จ', error);
    }
  }

  async onLogout(req, res) {
    try {
      const { authorization } = req.headers;
      await authenModel.onLogout({ accessToken: authorization });
      success(res, 'ออกจากระบบสำเร็จ');
    } catch (error) {
      failed(req, res, 'ออกจากระบบไม่สำเร็จ', error);
    }
  }

  async onGenerateAuth(req, res) {
    try {
      const exp = moment().add(10, 'minutes').valueOf();
      const encObj = {
        ref: '-',
        dplus_id: 'ZT00000',
        user_id: 'ZT00000',
        expired_date: exp,
        url_login: '-',
      };
      const p = path.join(__dirname + '../../../utils/key/auth/publicKey.pem');
      const publicKey = readFileSync(p);
      const encrypt = encryptedData(publicKey, JSON.stringify(encObj));
      success(res, 'ดึงข้อมูลสำเร็จ', {
        auth: encrypt.toString('base64'),
      });
    } catch (error) {
      failed(req, res, 'เกิดข้อผิดพลาด ดึงข้อมูลไม่สำเร็จ', error);
    }
  }
}

module.exports = new authenController();
