const { promisify } = require('util');
const fs = require('fs');
const mv = promisify(fs.rename);

exports.move = async (source, dir) => {
  await mv(source, dir);
};
