const uploadRouter = require('express')['Router']();
const { success } = require('../../config/response.js');
const { findDirectoryPath } = require('../../functions/index.js');
const multer = require('../../middleware/multer.js');
const path = require('path');
const { mkdirSync, existsSync } = require('fs');
const { move } = require('./uploadFn.js');

uploadRouter.post('/', multer().array('files', 10), async (req, res) => {
  const result = [];
  const { type, id } = req.body;
  for (let x = 0; x < req.files.length; x++) {
    const pathName = findDirectoryPath(type[x]);
    const dir = path.join(__dirname, `../../../public/${pathName}/`);
    if (!existsSync(dir)) {
      mkdirSync(dir, { recursive: true });
    }

    const val = req.files[x];
    const p = `${dir}${val.filename}`;
    result.push({
      id: id[x],
      type: type[x],
      path: `${pathName}/${val.filename}`,
    });
    await move(val.path, p);
  }
  success(res, 'อัปโหลดสำเร็จ', result);
});

module.exports = uploadRouter;
