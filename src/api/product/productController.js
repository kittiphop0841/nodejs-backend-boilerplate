const { failed, success } = require('../../config/response.js');
const productModel = require('./productModel.js');

class productController {
  async onSearch(req, res) {
    try {
      const { page, limit } = req.query;
      const query = await productModel.onSearch({ page, limit });
      success(res, 'ดึงข้อมูลสำเร็จ', {
        products: query,
      });
    } catch (error) {
      failed(req, res, 'ดึงข้อมูลไม่สำเร็จ', error);
    }
  }

  async onGetAll(req, res) {
    try {
      const query = await productModel.onSearch({ page: 1, limit: 100000 });
      success(res, 'ดึงข้อมูลสำเร็จ', {
        products: query,
      });
    } catch (error) {
      failed(req, res, 'ดึงข้อมูลไม่สำเร็จ', error);
    }
  }

  async onCreate(req, res) {
    try {
      await productModel.onCreate({ products: req.body.products });
      success(res, 'บันทึกข้อมูลสำเร็จ');
    } catch (error) {
      failed(req, res, 'บันทึกข้อมูลไม่สำเร็จ', error);
    }
  }
}

module.exports = new productController();
