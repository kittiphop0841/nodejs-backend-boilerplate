const { knex } = require('../../knex');
class productModel {
  onSearch(o) {
    const { page, limit } = o;
    return knex.raw(`EXEC DDMS_Product_GetSearch ?, ?`, [page, limit]);
  }

  onCreate(o) {
    const { products } = o;
    const prds = products.map((e) => `('${e.productName}')`).join(',');
    return knex.raw(`
      DECLARE @temp DDMD_Product
      INSERT INTO @temp (productName) VALUES ${prds}
      EXEC DDMS_Product_Create @temp`);
  }
}

module.exports = new productModel();
