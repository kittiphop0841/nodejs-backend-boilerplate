const Joi = require('joi');

exports.schemaGetSearch = Joi.object().keys({
  page: Joi.number().min(1).required(),
  limit: Joi.number().min(5).max(1000).required(),
});

exports.schemaCreate = Joi.object().keys({
  products: Joi.array().items({
    productName: Joi.string().required(),
  }),
});
