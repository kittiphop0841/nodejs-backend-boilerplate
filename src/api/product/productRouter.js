const productRouter = require('express')['Router']();
const { validateSchema } = require('../../middleware/validate-schema.js');
const productController = require('./productController.js');
const { schemaGetSearch, schemaCreate } = require('./productSchema.js');

productRouter.get(
  '/search',
  validateSchema([schemaGetSearch], 'query'),
  productController.onSearch,
);

productRouter.get('/get-all', productController.onGetAll);

productRouter.post(
  '/create',
  validateSchema([schemaCreate]),
  productController.onCreate,
);

module.exports = productRouter;
