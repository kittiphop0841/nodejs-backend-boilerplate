const winston = require('winston');
const logstash = require('winston3-logstash-transport');
const { env } = require('../env');

const MESSAGE = Symbol.for('message');
const LEVEL = Symbol.for('level');

const errorToLog = (log) => {
  const formatted = {
    message: null,
    level: 'error',
  };

  formatted[LEVEL] = 'error';
  if (log.message) {
    formatted.message = `${log.message}: ${log.stack}`;
  } else {
    formatted.message = log.stack;
  }
  return formatted;
};

const errorFormatter = (logEntry) => {
  if (logEntry instanceof Error) {
    return errorToLog(logEntry);
  }

  if (logEntry.stack) {
    logEntry.message = `${logEntry.message}: ${logEntry.stack}`;
  }

  if (logEntry.message && typeof logEntry.message === 'object') {
    if (logEntry.message?.err instanceof Error) {
      return errorToLog(logEntry.message.err);
    } else {
      logEntry.message = JSON.stringify(logEntry.message);
    }
  }
  return logEntry;
};

const consoleTransport = new winston.transports.Console({
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.cli({
      colors: {
        error: 'red',
        warn: 'yellow',
        info: 'blue',
        http: 'green',
        verbose: 'cyan',
        debug: 'white',
      },
    }),
  ),
  handleExceptions: true,
});

const logstashTransport = new logstash({
  host: '192.168.2.81',
  port: 5044,
  mode: 'tcp',
  applicationName: `${env.appName || env.name}`.toLowerCase(),
  formatted: false,
});

const envTag = (logEntry) => {
  const tag = {
    env: env.node || 'local',
  };
  const taggedLog = Object.assign(tag, logEntry);
  logEntry[MESSAGE] = JSON.stringify(taggedLog);
  return logEntry;
};

const transports = [];
transports.push(consoleTransport);
transports.push(logstashTransport);

const logger = winston.createLogger({
  level: process.env.LOG_LEVEL || 'info',
  format: winston.format.combine(
    winston.format(errorFormatter)(),
    winston.format(envTag)(),
  ),
  transports,
});

logger.stream = {
  write: function (message) {
    logger.http(message);
  },
};

module.exports = logger;
