module.exports = {
  saltRounds: 10,
  ignoreCheckToken: [
    '/authen/login-google',
    '/authen/login',
    '/authen/generate',
  ],
};
