const _debug = require('debug')('debugging:debug');
const _error = require('debug')('debugging:error');
const { env } = require('../env');
const { handleLogger } = require('../logs');

exports.debug = _debug;
exports.err = (message, url) => {
  if (`${env.node}`.toUpperCase() === 'PRODUCTION') {
    handleLogger(message, url);
  }
  _error(`error with message %o`, message);
};
