const fs = require('fs');
const path = require('path');
const moment = require('moment');
const { env } = require('../env');
require('dotenv').config();

exports.getOriginPath = (originalUrl) => {
  let replace = originalUrl.replace(`${env.prefix}/api/v1/`, '');
  if (replace.includes('?')) {
    const length = replace.length;
    const str = replace;
    replace = '';
    for (let x = 0; x < length; x++) {
      if (str[x] === '?') {
        break;
      }
      replace += str[x];
    }
  }
  return replace;
};

exports.getCurrentDate = (format = 'YYYY-MM-DD') => {
  return moment().format(format);
};

exports.getDiffDate = (start, end, type = 'hour') => {
  const a = moment(start);
  const b = moment(end);
  return b.diff(a, type);
};

exports.unlinkFile = async (sourcePath, files = []) => {
  try {
    await Promise.all(
      files.map((f) => {
        const dir = path.join(
          __dirname,
          `../../public${sourcePath}/${f.filename}`,
        );
        fs.unlink(dir, (error) => error && console.log(error));
        return true;
      }),
    );
  } catch (error) {
    return [];
  }
};

exports.findType = (mimetype) => {
  switch (mimetype) {
    case 'image/png':
    case 'image/jpeg':
    case 'image/svg':
    case 'image/jpg':
      return 'images';
    default:
      return 'files';
  }
};

exports.findDirectoryPath = (p) => {
  switch (p) {
    case 'profile':
      return '/images/profile';
  }
};
