const crypto = require('crypto');

exports.decryptedData = (privateKey, toDecrypt) => {
  try {
    const buffer = Buffer.from(toDecrypt, 'base64');
    const decrypted = crypto.privateDecrypt(privateKey, buffer);
    return { success: true, ...JSON.parse(decrypted.toString('utf8')) };
  } catch (error) {
    console.log(error);
    return { success: false };
  }
};

exports.encryptedData = (publicKey, data) =>
  crypto.publicEncrypt(publicKey, Buffer.from(data, 'utf8'));
