require('../config_env');
const { getOsEnv } = require('./lib/env/utils');

exports.env = {
  appName: getOsEnv('APP_NAME'),
  name: getOsEnv('APP_NAME'),
  sign: getOsEnv('SIGN'),
  node: process.env.NODE_ENV,
  port: process.env.EXPRESS_PORT,
  mode: process.env.NODE_ENV === 'production' ? 'prod' : 'dev',
  log: {
    level: getOsEnv('LOG_LEVEL'),
  },
  db: {
    prd: {
      host: getOsEnv('HOST_DATABASE'),
      port: process.env.PORT_DATABASE || 3306,
      username: getOsEnv('USERNAME_DATABASE'),
      password: getOsEnv('PASSWORD_DATABASE'),
      database: getOsEnv('NAME_DATABASE'),
      instance: getOsEnv('INSTANCE_NAME'),
    },
    dev: {
      host: getOsEnv('HOST_DATABASE_DEV'),
      port: process.env.PORT_DATABASE_DEV || 3306,
      username: getOsEnv('USERNAME_DATABASE_DEV'),
      password: getOsEnv('PASSWORD_DATABASE_DEV'),
      database: getOsEnv('NAME_DATABASE_DEV'),
    },
  },
  saltround: getOsEnv('SALTROUND'),
  prefix: getOsEnv('PREFIX'),
  version_api: getOsEnv('VERSION_PATH'),
};
