const axios = require('axios');
const { env } = require('../env');

const URL_DEFALT_LARK = env.lark_default;

exports.MONITOR_LARK = async (message) => {
  const res = await axios(URL_DEFALT_LARK, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: JSON.stringify({
      title: 'มีเคสจ้า สนใจหนูหน่อย',
      text: message,
    }),
  });

  return res.data;
};
