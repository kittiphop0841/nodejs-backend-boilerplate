const { env } = require('../env');

require('dotenv').config();

module.exports = {
  production: {
    client: 'mssql',
    connection: {
      host: env.db.prd.host,
      user: env.db.prd.username,
      password: env.db.prd.password,
      database: env.db.prd.database,
      options: {
        instanceName: env.db.prd.instance,
        trustConenection: true,
      },
    },
    pool: {
      min: 0,
      max: 10,
    },
  },
  development: {
    client: 'mssql',
    useNullAsDefault: true,
    connection: {
      host: env.db.dev.host,
      user: env.db.dev.username,
      password: env.db.dev.password,
      database: env.db.dev.database,
    },
    pool: {
      min: 0,
      max: 10,
    },
  },
};
