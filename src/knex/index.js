const { debug } = require('../config/debug');
const { env } = require('../env');
const config_db = require('./knexfile');
const environ = env.node;
let knex = null;

if (environ !== 'xxx' && config_db[environ].connection.host !== 'xxx') {
  knex = require('knex')(config_db[environ]);
}

const testConnect = async () => {
  try {
    if (env.db.dev.host !== 'xxx') {
      await Promise.all([knex.raw('select 1+1 as result')]);
      return debug(`====== CONNECT DATABASE SUCCESS ======`);
    }
    debug(`!!! PLEASE CONFIG DATABASE !!!`);
  } catch (error) {
    console.log(error);
    debug(`====== CONNECT DATABASE FALIED ======`);
  }
};

testConnect();

module.exports = {
  knex,
};
