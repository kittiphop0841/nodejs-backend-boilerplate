const jsonwebtoken = require('jsonwebtoken');
const { promisify } = require('util');
const { failed } = require('../config/response');
const { getOriginPath } = require('../functions');
const { ignoreCheckToken } = require('../utils');
const { readFileSync } = require('fs');
const path = require('path');
const authenModel = require('../api/authen/authenModel');

exports.validateToken = () => {
  return async (req, res, next) => {
    /**
     * TODO CHECK AUTHORIZATION
     */

    const origin = getOriginPath(req.originalUrl);
    const checkIgnore = ignoreCheckToken.indexOf(origin) >= 0;
    if (checkIgnore) {
      return next();
    }

    if (req.headers && req.headers.authorization) {
      try {
        const pathFileKey = path.join(
          __dirname,
          '../utils/key/sign/publicKey.pem',
        );
        const publicKey = readFileSync(pathFileKey);
        const verify = promisify(jsonwebtoken.verify);

        const query = (
          await authenModel.onCheckAccessToken({
            accessToken: req.headers.authorization,
          })
        )[0];

        if (!query || !query.success) {
          return failed(req, res, 'ไม่สามารถใช้งานได้ (99989)');
        }

        const decode = await verify(req.headers.authorization, publicKey);
        for (const key in decode) {
          if (Object.hasOwnProperty.call(decode, key)) {
            const element = decode[key];
            req[key] = element;
          }
        }

        next();
      } catch (error) {
        failed(req, res, 'ไม่สามารถใช้งานได้ กรุณาติดต่อ admin');
      }
    } else {
      failed(req, res, 'token not found');
    }
  };
};
