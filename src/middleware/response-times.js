const moment = require('moment');
const { debug } = require('../config/debug');
const { env } = require('../env');
const logger = require('../logs/logging');

const getRequestBody = (req) => {
  const { headers, body } = req;
  return /multipart/i.test(headers['content-type']) ? 'skipped' : body;
};

const getResponseBody = (res, cb) => {
  const oldWrite = res.write;
  const oldEnd = res.end;
  const chunks = [];

  res.write = function (chunk) {
    if (chunk && Buffer.isBuffer(chunk)) chunks.push(chunk);
    oldWrite.apply(res, arguments);
  };

  res.end = function (chunk) {
    if (chunk && Buffer.isBuffer(chunk)) chunks.push(chunk);
    oldEnd.apply(res, arguments);
    cb(chunks.length ? chunks : chunk);
  };
};

const toJson = (string) => {
  try {
    return JSON.parse(string);
  } catch (e) {
    return string;
  }
};

function logResponseTime(req, res, next) {
  const { route, originalUrl, hostname } = req;
  const logObj = {
    host: hostname,
    etc: {
      requestId: req.requestId,
    },
    paramsBody: null,
    requestBody: {},
    route: route ? req.baseUrl + route.path : originalUrl,
  };

  const startHrTime = process.hrtime();
  debug(
    `%s start origin (${process.pid}) %o`,
    moment().format('YYYY-MM-DD HH:mm:ss'),
    req.originalUrl,
  );

  getResponseBody(res, (chunks) => {
    logObj['statusCode'] = res.statusCode;
    if (!/utf-8/i.test(res.getHeaders()['content-type'])) {
      logObj['responseBody'] = {};
    } else if (Array.isArray(chunks)) {
      logObj['responseBody'] = toJson(Buffer.concat(chunks).toString('utf8'));
    } else {
      logObj['responseBody'] = toJson(chunks);
    }
  });

  res.on('finish', () => {
    const { params } = req;
    const elapsedHrTime = process.hrtime(startHrTime);
    const elapsedTimeInMs = elapsedHrTime[0] * 1000 + elapsedHrTime[1] / 1e6;
    logObj.etc['timeToFirstByte'] = elapsedTimeInMs;
    logObj.paramsBody = params;
    logObj.requestBody = getRequestBody(req);

    debug(
      `%s origin (${req.requestId}:${process.pid}) %o %s`,
      moment().format('YYYY-MM-DD HH:mm:ss'),
      req.originalUrl,
      `\u001b[31m${parseFloat(elapsedTimeInMs).toFixed(2)}ms\u001b[0m`,
    );

    env.node === 'production' && logger.info(logObj);
  });

  next();
}

module.exports = logResponseTime;
