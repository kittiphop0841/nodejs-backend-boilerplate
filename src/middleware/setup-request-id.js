const cuid = require('cuid');
exports.requestId = (req, res, next) => {
  req.requestId = cuid();
  next();
};
