const { existsSync, mkdirSync } = require('fs');
const multer = require('multer');
const path = require('path');

const storage = (p) =>
  multer.diskStorage({
    destination: async function (_req, _file, cb) {
      if (!p) {
        p = 'tmp';
      }
      const join = path.join(__dirname, `../../public/${p}`);
      if (!existsSync(join)) {
        mkdirSync(join);
      }

      cb(null, join);
    },
    filename: (req, file, cb) => {
      if (file.originalname.match(/\.(PNG|JPG|JPEG)/gi)) {
        const split = file.originalname.split('.').length;
        const fileType = file.originalname.split('.')[split - 1];
        const filename = `${Date.now()}.${fileType}`;
        req.fileName = filename;
        cb(null, filename);
      } else {
        cb(null, null);
      }
    },
  });

module.exports = (p) => multer({ storage: storage(p) });
