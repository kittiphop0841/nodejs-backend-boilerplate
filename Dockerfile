FROM node:14-alpine
ENV TZ=Asia/Bangkok

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

RUN npm config set unsafe-perm true
RUN npm install pm2 -g

COPY . /usr/src/app

RUN echo -e "https://dl-cdn.alpinelinux.org/alpine/v3.11/main\nhttps://dl-cdn.alpinelinux.org/alpine/v3.11/community" > /etc/apk/repositories
RUN rm -rf /var/cache/apk/* && \
  rm -rf /tmp/*
RUN apk update
RUN apk --no-cache add ca-certificates \
  && apk add --update tzdata \
  && cp /usr/share/zoneinfo/Asia/Bangkok /etc/localtime \
  && apk del tzdata

EXPOSE 3989

CMD ["/bin/sh", "-c", "pm2-runtime --name 'DEMO' 'npm start'"]
