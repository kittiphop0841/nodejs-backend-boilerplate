require('./config_env');
const express = require('express');
const bodyparser = require('body-parser');
const cors = require('cors');
const ratelimit = require('express-rate-limit');
const helmet = require('helmet');
const path = require('path');
const logMiddleware = require('./src/lib/express_stackdriver');
const { existsSync } = require('fs');
const { createApi } = require('./src/api');
const { debug } = require('./src/config/debug');
const { env } = require('./src/env');
const { requestId } = require('./src/middleware/setup-request-id');
const logstashResponseTime = require('zt-elk-logging/response-time');

require('moment/locale/th');
require('dotenv').config();

const app = express();

/*###################### SETTING ######################*/
app.use(helmet());
app.use(requestId);
app.use('/api/v1/static', express.static(path.join(__dirname, './public')));

const whitelist = [
  undefined,
  'http://localhost:8080',
  'https://demo.zetta-system.com',
];

if (!env.appName && !env.name) {
  throw new Error('Please config env.name or APP_NAME in .env');
}

const corsOption = {
  origin: (origin, cb) => {
    origin && debug('origin %o', origin);
    if (whitelist.indexOf(origin) !== -1) {
      cb(null, true);
    } else {
      cb(new Error('Not allows by Cors'));
    }
  },
  optionsSuccessStatus: 200,
  credentials: true,
};

app.use(cors(corsOption));
app.use(bodyparser.urlencoded({ extended: true, limit: '50mb' }));
app.use(bodyparser.json({ limit: '50mb' }));

const limiter = ratelimit({
  windowsMs: 60 * 1000,
  max: 200,
});

app.use(limiter);

/*###################### DEBUG ######################*/
app.use(logstashResponseTime);

/*###################### CONFIG MONITOR ######################*/

app.use(`/${env.prefix}${env.version_api}/health`, (req, res) => {
  const { from } = req.query;
  from && debug(`Check Health FROM ${from}`);
  res.status(200).json({ success: true, status: 'Available' });
});

const credentialsPath = path.join(
  __dirname,
  './config/google_credentials.json',
);
if (!existsSync(credentialsPath)) {
  debug(`!!! PLEASE GOOGLE LOG !!!`);
} else {
  debug(`### START GOOGLE LOG SUCCESS ###`);
  app.use(
    logMiddleware({
      local: false,
      keyFilename: credentialsPath,
      ignoreRoute: ['/ignore/:data'],
      ignoreBody: ['*'],
      maxEntrySize: 500000,
    }),
  );
}

/*###################### CREATE API ######################*/
// try {
//   app.use(requestID());
// } catch (error) {
//   console.log(error);
// }
createApi(app);

switch (`${env.node}`.toUpperCase()) {
  case 'DEVELOPMENT':
    app.listen(env.port, () => {
      debug(`[${env.node}] ::: Server is running port ${env.port}`);
    });
    break;
  case 'PRODUCTION':
    const cluster = require('cluster');
    const { length } = require('os').cpus();
    const numCPUs = length > 4 ? 1 : length;
    if (cluster.isMaster) {
      for (let i = 0; i < numCPUs; i++) {
        cluster.fork();
      }
      debug(`Server is running port (${numCPUs} CPUs) : ${env.port}`);
      cluster.on('exit', (worker) =>
        console.log(`worker ${worker.process.pid} died`),
      );
    } else {
      app.listen(env.port, () =>
        debug(`Server is running port (${process.pid}): ${env.port}`),
      );
    }
    break;
}
