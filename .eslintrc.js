module.exports = {
  parserOptions: {
    ecmaVersion: 2021,
  },
  env: {
    es6: true,
  },
  root: true,
  extends: ['plugin:prettier/recommended'],
  rules: {
    'no-var': 2,
    'no-unused-vars': 2,
    'no-alert': 2,
  },
};
